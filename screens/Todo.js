import React from 'react';

import { gql } from 'apollo-boost';
import { Text, View } from 'react-native';
import { Appbar } from 'react-native-paper';

import { useQuery } from '@apollo/react-hooks';

import { Todos } from '../components/Todos';

export const GET_TODOS = gql`
  query getMyTodos {
    todos {
      id
      name
      is_completed
      added_date
    }
  }
`;

const Loading = () => (
  <View>
    <Text>Loading...</Text>
  </View>
);

const Error = ({ children }) => {
  return (
    <View>
      <Text>{children}</Text>
    </View>
  );
};
const Todo = () => {
  const { loading, error, data } = useQuery(GET_TODOS, {
    fetchPolicy: 'cache-first',
  });

  if (loading) {
    return <Loading />;
  }
  if (error) {
    return <Error>{error}</Error>;
  }
  return (
    <>
      <Appbar.Header>
        <Appbar.Content title="My CI/CD TODO List" />
      </Appbar.Header>
      <Todos.Input />
      <Todos.List todos={data.todos} />
    </>
  );
};

export default Todo;
