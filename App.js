import React from 'react';

import { ApolloClient, InMemoryCache } from 'apollo-boost';
import { sub } from 'date-fns';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';

import { ApolloProvider } from '@apollo/react-hooks';

import { resolvers } from './graphql/mutations';
import Todo from './screens/Todo';

const cache = new InMemoryCache();
const client = new ApolloClient({
  cache,
  resolvers,
});
cache.writeData({
  data: {
    todos: [
      {
        id: 6,
        name: 'Configurare pipelines Gitlab!',
        is_completed: false,
        added_date: sub(new Date(), { days: 3 }).toISOString(),
        __typename: 'todo',
      },
      {
        id: 5,
        name: 'Initializare Fastlane pentru Android',
        is_completed: false,
        added_date: sub(new Date(), { days: 2, hours: 10 }).toISOString(),
        __typename: 'todo',
      },
      {
        id: 4,
        name: 'Initializare Fastlane pentru iOS',
        is_completed: false,
        added_date: sub(new Date(), { days: 3 }).toISOString(),
        __typename: 'todo',
      },
      {
        id: 3,
        name: 'Setare cache only',
        is_completed: true,
        added_date: sub(new Date(), { days: 3 }).toISOString(),
        __typename: 'todo',
      },
      {
        id: 2,
        name: 'Setare GraphQL',
        is_completed: true,
        added_date: sub(new Date(), { days: 4 }).toISOString(),
        __typename: 'todo',
      },
      {
        id: 1,
        name: 'Initializat proiectul React Natvie',
        is_completed: true,
        added_date: sub(new Date(), { days: 5 }).toISOString(),
        __typename: 'todo',
      },
    ],
  },
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <PaperProvider>
        <SafeAreaView style={styles.safeArea}>
          <Todo />
        </SafeAreaView>
      </PaperProvider>
    </ApolloProvider>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
});
export default App;
