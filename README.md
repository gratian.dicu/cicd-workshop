# CI/CD Workshop/presentation repository

## Scope

Main focus is to offer a way to automate React Native builds to integrate and deliver them via multiple distribution channels (Firebase, Testflight, Google Play).\
This is done by showing tools used, ways to achieve different scenarios and ways to integrate all them.

## Structure
The presentations is based on starting with a branching and getting the setup to a certain point where the audience is able to understand. The end result usually is in a *-end branch.

* step-gitlab
* step-gitlab-end
* step-fastlane
* step-fastlane-end
* step-complex
* step-complex-end

Due to some time constraint some branches contain more data then presented, like Firebase iOS and Android setup, steps that are easy to follow when registering a new application in Firebase




