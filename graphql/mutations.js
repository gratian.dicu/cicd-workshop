import { GET_TODOS } from '../screens/Todo';

export const resolvers = {
  Mutation: {
    addTodo: (_, todo, { cache }) => {
      const { todos } = cache.readQuery({ query: GET_TODOS });
      let new_todo = {
        id: Math.random() * 10000,
        name: todo.name,
        is_completed: false,
        added_date: new Date().toString(),
        __typename: 'todo',
      };
      cache.writeData({
        data: {
          todos: [new_todo, ...todos],
        },
      });

      return new_todo;
    },
    toggleTodo: (_, { id }, { cache }) => {
      const existingTodos = cache.readQuery({ query: GET_TODOS });
      const newTodos = existingTodos.todos.map((t) => {
        if (t.id === id) {
          return { ...t, is_completed: !t.is_completed };
        } else {
          return t;
        }
      });
      cache.writeData({
        data: {
          todos: newTodos,
        },
      });

      return newTodos;
    },
  },
};
