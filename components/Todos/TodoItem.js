import React, { memo } from 'react';

import { gql } from 'apollo-boost';
import { formatDistance } from 'date-fns';
import { Checkbox, Divider, List } from 'react-native-paper';

import { useMutation } from '@apollo/react-hooks';

const TOGGLE_TODO = gql`
  mutation toggleTodo($id: String!) {
    toggleTodo(id: $id) @client {
      id
    }
  }
`;

const TodoItem = ({ todo: { name, id, added_date, is_completed } }) => {
  const [toggleTodoMutation] = useMutation(TOGGLE_TODO);

  const toggleTodo = () => {
    toggleTodoMutation({
      variables: { id: id },
    });
  };
  const date = formatDistance(new Date(added_date), new Date());

  return (
    <>
      <List.Item
        title={name}
        description={date}
        onPress={toggleTodo}
        left={() => (
          <Checkbox status={is_completed ? 'checked' : 'unchecked'} onPress={toggleTodo} />
        )}
      />
      <Divider />
    </>
  );
};

export default memo(TodoItem);
