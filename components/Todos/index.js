import TodoInput from './TodoInput';
import TodoItem from './TodoItem';
import TodoList from './TodoList';

export const Todos = {
  Input: TodoInput,
  Item: TodoItem,
  List: TodoList,
};
