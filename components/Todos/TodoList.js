import React from 'react';
import { FlatList } from 'react-native';
import { Todos } from '.';

const TodoList = ({ todos }) => {
  return (
    <FlatList
      data={todos}
      keyExtractor={(todo) => todo.id.toString()}
      renderItem={({ item }) => {
        return <Todos.Item todo={item} />;
      }}
    />
  );
};

export default TodoList;
