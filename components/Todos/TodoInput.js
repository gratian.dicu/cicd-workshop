import React, { useState } from 'react';

import { gql } from 'apollo-boost';
import { StyleSheet, View } from 'react-native';
import { FAB, TextInput } from 'react-native-paper';

import { useMutation } from '@apollo/react-hooks';

const ADD_TODO = gql`
  mutation addTodo($name: String!) {
    addTodo(name: $name) @client {
      name
    }
  }
`;

const TodoInput = () => {
  const [todoInput, setTodoInput] = useState('');
  const [addTodoMutation] = useMutation(ADD_TODO);

  const addTodo = () => {
    addTodoMutation({
      variables: {
        name: todoInput,
      },
    });
    setTodoInput('');
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="add a Todo"
        onChangeText={(text) => setTodoInput(text)}
        value={todoInput}
      />
      <FAB small icon="plus" style={styles.fab} onPress={addTodo} disabled={todoInput === ''} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 8,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default TodoInput;
